package com.firewolf.rfid

import android.net.Uri
import android.os.Environment
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.core.net.toFile
import com.bumptech.glide.Glide
import java.io.File
import java.io.FileInputStream
import java.io.FileOutputStream

open class globalActivity: AppCompatActivity() {

    override fun onResume() {
        super.onResume()
        window.statusBarColor = ContextCompat.getColor(this, R.color.colorPrimaryDark)
        val flags = (View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
                or View.SYSTEM_UI_FLAG_HIDE_NAVIGATION)
        /*window.decorView.systemUiVisibility = flags*/
    }

    fun saveAvatar(id: String, uri: Uri){
        if(!isExternalStorageWritable){
            Toast.makeText(this, "儲存裝置異常，無法儲存頭像", Toast.LENGTH_SHORT).show()
            return
        }

        val dir = getAvatarAlbumPath()
        val file = File(dir, "ava_$id.jpg")
        val input = FileInputStream(uri.toFile())
        input.use {
            val output = FileOutputStream(file)
            output.use {
                val b = ByteArray(1024)
                var j = input.read(b)
                while(j > 0){
                    output.write(b, 0, j)
                    j = input.read(b)
                }
            }
        }
    }

    fun getAvatarAlbumPath(): File {
        val albumName: String = "avatars"
        val file = File(getExternalFilesDir(Environment.DIRECTORY_PICTURES), albumName)
        if (!file.mkdirs()) {
            Log.e("", "Directory not created or exist")
        }
        return file
    }

    private val isExternalStorageReadable: Boolean
        get() {
            val state = Environment.getExternalStorageState()
            return Environment.MEDIA_MOUNTED == state || Environment.MEDIA_MOUNTED_READ_ONLY == state
        }
    val isExternalStorageWritable: Boolean
        get() {
            val state = Environment.getExternalStorageState()
            return Environment.MEDIA_MOUNTED == state
        }
}