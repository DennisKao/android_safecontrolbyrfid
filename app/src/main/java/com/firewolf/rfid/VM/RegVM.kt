package com.firewolf.rfid.VM

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class RegVM : ViewModel() {
    private val _rfid: MutableLiveData<String> = MutableLiveData<String>()
    val rfid: LiveData<String>
        get() = _rfid

    init {
        _rfid.value = ""
    }

    fun load(memId: String){
        _rfid.value = memId
    }


}