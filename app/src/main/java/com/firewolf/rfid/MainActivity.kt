package com.firewolf.rfid

import android.Manifest
import android.content.DialogInterface
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.LinearLayout
import android.widget.PopupWindow
import android.widget.Toast
import android.widget.Toast.*
import androidx.appcompat.app.AlertDialog
import androidx.core.app.ActivityCompat
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.SimpleItemAnimator
import com.dk.bleNfc.BleNfcDeviceService
import com.firewolf.rfid.Manager.FiremenManager
import com.firewolf.rfid.Manager.InOutManager
import com.firewolf.rfid.Manager.QueueManager
import com.firewolf.rfid.Manager.TeamManager
import com.firewolf.rfid.R.drawable.*
import com.firewolf.rfid.Util.GPSUtil
import com.firewolf.rfid.Util.NFCTool
import com.firewolf.rfid.Util.NFCTool.NFCDeviceStatusListener
import com.firewolf.rfid.Util.NFCTool.PERMISSION_REQUEST_COARSE_LOCATION
import com.firewolf.rfid.ViewControl.MainActivity.TeamControlAdapter
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.mem_maintain_popwin.view.*



class MainActivity() : globalActivity() {

    private lateinit var recyclerView: RecyclerView
    private lateinit var viewManager: RecyclerView.LayoutManager
    private val tm: TeamManager by lazy {(applicationContext as mApp).tm}
    private val fm: FiremenManager by lazy {(applicationContext as mApp).fm}
    private val qm: QueueManager by lazy {(applicationContext as mApp).qm}
    private val iom: InOutManager by lazy {(applicationContext as mApp).iom}
    private val nfcTool: NFCTool? by lazy {(applicationContext as mApp).nfcTool}
    private val pop: PopupWindow by lazy {PopupWindow(this)}


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        //this.requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE

        //Open Gps
        GPSUtil.openGPS(this)

        //Get Permission
        if(checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED){
            requestPermissions(arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE), 2)
        }
        if (checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(
                this,
                arrayOf(Manifest.permission.ACCESS_COARSE_LOCATION),
                PERMISSION_REQUEST_COARSE_LOCATION
            )
        }

        nfcTool?.also{
            //Bind Service
            val gattServiceIntent = Intent(this, BleNfcDeviceService::class.java)
            bindService(gattServiceIntent, it.mServiceConnection, BIND_AUTO_CREATE)
            //register NFC Event, 控制藍芽連線狀態
            it.setDeviceStatusListener(mNFCDeviceStatusListener())
        }

        recyclerView = recycle.apply {
//            layoutManager = viewManager
            adapter = TeamControlAdapter(tm, iom, qm)
            recycledViewPool.setMaxRecycledViews(0, 5)//設置此參數解決卡頓問題，造成卡頓的原因在於cell最大數過低，導致cell被回收
            val sa = (this.itemAnimator) as SimpleItemAnimator
            sa.supportsChangeAnimations = false
        }

        memMaintainBtn.setOnClickListener(MemMaintainBtnClickEvent())
        mainCreateTeamBtn.setOnClickListener {
            CreateTeamDialog().show()
        }
    }

    override fun onResume() {
        super.onResume();
        iom.switchMode(InOutManager.STATE.INOUT)
        nfcTool?.resume()

        //startActivity(Intent(this@MainActivity, RegisterActivity::class.java))
    }

    override fun onDestroy() {
        super.onDestroy()
        pop.dismiss()
        nfcTool?.let {
            unbindService(it.mServiceConnection);
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String?>,
        grantResults: IntArray) {
        when (requestCode) {
            PERMISSION_REQUEST_COARSE_LOCATION -> {
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {  //申請權限成功
                    println("成功申請")
                    nfcTool?.also {
                        val gattServiceIntent = Intent(this, BleNfcDeviceService::class.java)
                        bindService(gattServiceIntent, it.mServiceConnection, BIND_AUTO_CREATE)
                    }
                }
            }
        }
    }

    inner class mNFCDeviceStatusListener: NFCDeviceStatusListener {

        var isOn = false

        override fun onDeviceConnected() { }//僅接收設備處於可運作狀態，不做任何的UI操作

        override fun onDeviceDisconnection() = change(0.0)

        override fun onDeviceSearching() = change(-1.0)

        override fun onDeviceHousekeeping(v: Double) = change( v)

        private fun change(voltage: Double){
            val batteryImg = getVoltageImg(voltage)

            when(voltage){
                //ing
                -1.0 -> updUI(bt_connecting, batteryImg, "連線中", "#5AC8FA")
                //dis
                0.0  -> updUI(bt_disconnect, batteryImg, "未連線", "#BFBFBF")
                //conn
                else -> updUI(bt_connected, batteryImg, "已連線", "#FFFFFF")
            }
        }

        private fun getVoltageImg(v: Double): Int{
            var i = R.drawable.battery_null
            i = when {
                v <= 0    -> R.drawable.battery_null
                v > 3.93  -> R.drawable.battery_3_3
                v > 3.77  -> R.drawable.battery_2_3
                else      -> R.drawable.battery_1_3
            }
            return i
        }

        private fun updUI(_bt_img: Int,
                          _bt_battery_img: Int,
                          _bt_status_text: String,
                          _bt_status_color: String) {
            runOnUiThread {
                bt_battery.setImageResource(_bt_battery_img)
                bt_img.setImageResource(_bt_img)
                bt_status.text = _bt_status_text
                bt_status.setTextColor(Color.parseColor(_bt_status_color))
            }
        }
    }

    /*處理人員管制系統內部按鈕事件，畫面切轉*/
    inner class MemMaintainBtnClickEvent: View.OnClickListener{

        var a = 0
        init{

            val view = LayoutInflater.from(this@MainActivity).inflate(R.layout.mem_maintain_popwin, null)
            view.setOnClickListener{
                pop.dismiss()
            }
            view.reg.setOnClickListener {
                startActivity(Intent(this@MainActivity, RegisterActivity::class.java))
                pop.dismiss()
            }
            view.inout.setOnClickListener {
                makeText(this@MainActivity, "inout", LENGTH_SHORT).show()
            }
            view.overview.setOnClickListener {
                makeText(this@MainActivity, "console", LENGTH_SHORT).show()
            }
            pop.setOnDismissListener {
                recycleDark.alpha = 0f
            }
            pop.contentView = view
            pop.animationStyle = R.style.memControlPopup
            pop.width = ViewGroup.LayoutParams.MATCH_PARENT
            pop.height = ViewGroup.LayoutParams.MATCH_PARENT

            pop.setBackgroundDrawable(ColorDrawable(0))
        }

        fun show(){
            pop.showAsDropDown(title_bar.rootView)
            recycleDark.alpha = 0.5f
        }

        fun unshow(){
            pop.dismiss()
        }

        override fun onClick(v: View?) {
            if(pop.isShowing)
                unshow()
            else
                show()
        }
    }

    inner class CreateTeamDialog: AlertDialog.Builder(this){
        init{
            val editText = EditText(context)
            val lp = LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT
            )
            editText.layoutParams = lp
            setView(editText)
            setTitle("創建隊伍")
            setMessage("輸入隊伍名稱")
            val confirm = DialogInterface.OnClickListener { d, _ ->
                if(editText.text.isEmpty())
                    Toast.makeText(context, "隊伍名稱不能為空", Toast.LENGTH_SHORT).show()
                else{
                    tm.createTeam(editText.text.toString())
                }
            }
            setPositiveButton("確認", confirm)
            setNeutralButton("取消", DialogInterface.OnClickListener{ _, _ -> })
        }
    }
}

