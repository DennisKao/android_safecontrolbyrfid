package com.firewolf.rfid.Manager

import com.firewolf.rfid.module.FiremenInfo
import com.firewolf.rfid.module.Team
import com.firewolf.rfid.module.TeamEntity
class TeamManager(val qm: QueueManager){

    interface TeamManagerListener{
        fun onTeamDeleted(index: Int)
        fun onTeamCreated(team: Team, index: Int)
        fun onMemberAddedToTeam(memIndex: Int, teamIndex: Int)
        fun onMemberRemovedFromTeam(memIndex: Int, teamIndex: Int)
    }

    var _listener: TeamManagerListener? = null

    private var serialNum: Int = 0

    private val _teamList: MutableMap<String, TeamEntity> = mutableMapOf() // serial num, team
    val teamList: List<Team>
        @Synchronized
        get() = _teamList.values.toList()
    val size: Int
        @Synchronized
        get() = _teamList.size

    var selectedTeam: Team? = null

    fun createTeam(teamName: String): Team{
        val id = serialNum++.toString()
        val team = TeamEntity(
            TeamInfo(
                id = id,
                name = teamName,
                leader = null
            )
        )
        _teamList[id] = team
        _listener?.onTeamCreated(team, _teamList.size -1)
        return team
    }

    fun removeTeam(teamId: String): Boolean {
        _teamList.apply {
            get(teamId)?.let{team ->
                if(team.size > 0){
                    _listener?.onTeamDeleted(-1)
                    return false
                }else{
                    val index = keys.indexOf(teamId)
                    remove(teamId)
                    _listener?.onTeamDeleted(index)
                    return true
                }
            }
            return false
        }
    }

    fun getTeamInfo(teamId: String): TeamInfo? = _teamList[teamId]?.getInfo()

    fun addMember(teamId: String, fmInfo: FiremenInfo): Boolean{
        _teamList[teamId]?.let{ team ->
            team.addMember(fmInfo)
            _listener?.onMemberAddedToTeam(team.size-1, _teamList.keys.indexOf(teamId))
            return true
        }
        // maybeTODO:   不成功事件，提醒adapter 顯示 Toast 顯示使用者未選擇
        return false
    }

    // Notice: 若隊長為被刪除隊員，清空隊伍隊長職位後，刪除人並callback
    fun rmvMember(teamId: String, memId: String){
        _teamList[teamId]?.let { team ->
            if(team.getInfo().leader?.id == memId)
                team.getInfo().leader = null
            team.memList.apply {
                get(memId)?.let {
                    val index = keys.indexOf(memId)
                    team.removeMember(memId)
                    _listener?.onMemberRemovedFromTeam(index, _teamList.keys.indexOf(teamId))
                }
            }
        }
    }
}
