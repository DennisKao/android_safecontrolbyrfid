package com.firewolf.rfid.Manager

import com.firewolf.rfid.module.FiremenInfo

class FiremenManager {

    private val firemenList: MutableMap<String, FiremenInfo> = mutableMapOf();//id, info
    val size: Int
        get() = firemenList.size

    /***
     * if firemen is new, add or update
     */
    fun regUpdFiremen(info: FiremenInfo){
        firemenList[info.id] = info
    }

    fun removeFiremen(id: String){
        firemenList.remove(id)
    }

    fun getFireMen(id: String): FiremenInfo? {
        return firemenList[id]
    }

    @Synchronized
    fun contains(memId: String): Boolean = firemenList.containsKey(memId)

}