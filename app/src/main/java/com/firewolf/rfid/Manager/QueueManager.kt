package com.firewolf.rfid.Manager

import java.util.concurrent.ArrayBlockingQueue

class QueueManager {

    val BQueue: ArrayBlockingQueue<String> = ArrayBlockingQueue(5)
    val RQueue: ArrayBlockingQueue<String> = ArrayBlockingQueue(1)

}