package com.firewolf.rfid.Manager


import com.firewolf.rfid.module.FiremenDBInfo
import com.firewolf.rfid.module.FiremenInfo
import java.util.concurrent.TimeUnit
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

class InOutManager(
    val qm: QueueManager,
    val fm: FiremenManager,
    val tm: TeamManager
) {

    private val inOutList = mutableListOf<InOutRecTable>()
    private val loginList = mutableMapOf<String, String>() // memId, teamId
    private var state: STATE =
        STATE.LOGIN
    private val enable = true

    enum class STATE{
        LOGIN,//登入
        INOUT,//人員管制、人員進出
        REGISTER,//註冊
        STOP,
    }

    data class InOutRecTable(
        var inOut: String,//I  O
        var fmInfo: FiremenInfo,
        var ts   : Long
    )

    init{
        CoroutineScope(Dispatchers.IO).launch{
            delay(3000)
            repeat(50){
                Thread.sleep(250)
                //qm.BQueue.offer(it.toString())
            }
        }
        Thread {
            while (enable) {
                val cardId = qm.BQueue.poll(30, TimeUnit.SECONDS)
                if (cardId != null)
                    beeCardBy(cardId)
                    Thread.sleep(250)
            }
        }.start()
    }

    fun switchMode(state: STATE){
        this.state = state
    }

    fun beeCardBy(cardId: String){
        when(state){
            STATE.LOGIN -> {

            }

            STATE.INOUT -> {
                fm.getFireMen((cardId))?.let { fmInfo ->
                    val ts = System.currentTimeMillis()
                    // maybeTODO: 注意進入時間跟 氧氣啟動時間不同

                    loginList[cardId]?.let { teamId ->
                        // Notice: 消防人登出
                        loginList.remove(cardId)
                        inOutList.add((InOutRecTable(
                            "O",
                            fmInfo,
                            ts
                        )))
                        tm.rmvMember(teamId, cardId)

                    } ?: run{
                        // Notice: 消防人登入
                        val team = tm.selectedTeam
                        if(team != null){
                            val teamId = team.getInfo().id
                            fmInfo.init(ts)
                            if(tm.addMember(teamId, fmInfo)){
                                // Notice: 新增隊員成功
                                loginList[cardId] = teamId
                                // maybeTODO: 可能會因為O2startTime 都是同一個記憶體位置，而讓記錄出現問題，此問題需先釐清firebase結構為何
                                inOutList.add(
                                    InOutRecTable(
                                        "I",
                                        fmInfo,
                                        ts
                                    )
                                )
                            }
                        }
                    }
                } ?: run{
                    println("$cardId 沒註冊")
                    println("自動註冊在記憶體\\無入資料庫")
                    val fmInfo = FiremenDBInfo(
                        id = cardId,
                        name = "第${fm.size}卡",
                        number = "${fm.size}",
                        callSign = "沃卡",
                        department = "秘密部門"
                    )
                    val fm2 = fmInfo.toFiremenInfo()
                    fm.regUpdFiremen(fm2)
                }
            }
            STATE.REGISTER -> {
                qm.RQueue.offer(cardId)
            }
            else -> {}
        }
    }

}