package com.firewolf.rfid

import android.app.Application
import com.firewolf.rfid.Manager.FiremenManager
import com.firewolf.rfid.Manager.InOutManager
import com.firewolf.rfid.Manager.QueueManager
import com.firewolf.rfid.Manager.TeamManager
import com.firewolf.rfid.Module.*
import com.firewolf.rfid.Util.DAOTool
import com.firewolf.rfid.Util.NFCTool

public class mApp: Application() {
    val qm: QueueManager =
        QueueManager()
    val nfcTool: NFCTool = NFCTool("Dev_01", qm)
    //val nfcTool: NFCTool? = null
    val fm = FiremenManager()
    val tm = TeamManager(qm)
    val iom = InOutManager(qm, fm, tm)
    override fun onCreate() {
        super.onCreate()

        val t =Thread {
            DAOTool.MemberDatabase.getInstance(this@mApp)?.apply {
                memberDao().getAll().forEach {
                    fm.regUpdFiremen(it.fmDBInfo.toFiremenInfo())
                }
                println("當前消防人員註冊數量:   ${memberDao().getAll().size}")
            }
        }
        t.start()
        t.join()
    }
}