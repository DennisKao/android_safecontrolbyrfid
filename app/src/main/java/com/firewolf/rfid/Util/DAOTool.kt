package com.firewolf.rfid.Util

import android.content.Context
import androidx.room.*
import com.firewolf.rfid.module.FiremenDBInfo

class DAOTool{
    companion object{
        const val TABLE_NAME = "member"
    }

    @Entity(tableName = TABLE_NAME)
    data class Firemen(
        @PrimaryKey val _id: String,
        @Embedded val fmDBInfo: FiremenDBInfo
    )

    @Dao
        interface MemberDao{
        @Query("Select * From $TABLE_NAME")
        fun getAll(): List<Firemen>

        @Insert(onConflict = OnConflictStrategy.IGNORE)
        fun insertNewMember(firemen: Firemen): Long

        @Update(onConflict = OnConflictStrategy.REPLACE)
        fun updateMember(firemen: Firemen): Int
    }

    @Database(entities = [Firemen::class], version = 1)
    abstract class MemberDatabase: RoomDatabase(){
        abstract fun memberDao(): MemberDao

        companion object{
            private var INSTANCE: MemberDatabase? = null
            @Synchronized
            fun getInstance(context: Context): MemberDatabase?{
                if(INSTANCE == null) {
                    INSTANCE = Room.databaseBuilder(context,
                    MemberDatabase::class.java,
                    "kerneldb").build()
                    }

                return INSTANCE
            }

            fun destroyInstance(){
                INSTANCE = null
            }
        }
    }
}