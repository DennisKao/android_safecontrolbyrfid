package com.firewolf.rfid.Util.XPopup

import android.content.Context
import android.view.View
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.firewolf.rfid.R
import com.lxj.easyadapter.EasyAdapter
import com.lxj.easyadapter.MultiItemTypeAdapter.SimpleOnItemClickListener
import com.lxj.easyadapter.ViewHolder
import com.lxj.xpopup.core.AttachPopupView
import com.lxj.xpopup.interfaces.OnSelectListener
import com.lxj.xpopup.widget.VerticalRecyclerView

open class MemOptionPopupView(
    context: Context
): AttachPopupView(context){
    
    private val recyclerView: VerticalRecyclerView by lazy {findViewById<VerticalRecyclerView>(com.lxj.xpopup.R.id.recyclerView)}
    var selectListener: OnSelectListener? = null
    var data: List<String>? = null
    override fun getImplLayoutId(): Int = R.layout._xpopup_attach_impl_list

    override fun initPopupContent() {

        super.initPopupContent()
        data?.let { data ->
            val adapter: EasyAdapter<String> =
                object : EasyAdapter<String>(data, R.layout._xpopup_adapter_text) {

                    override fun bind(
                        holder: ViewHolder,
                        s: String,
                        position: Int
                    ) {
                        holder.setText(com.lxj.xpopup.R.id.tv_text, s)
                        (holder.getView<TextView>(com.lxj.xpopup.R.id.tv_text)).textSize = 18f
                        holder.itemView.layoutParams = LinearLayout.LayoutParams(
                            LayoutParams.MATCH_PARENT,
                            LayoutParams.WRAP_CONTENT
                        )
                        holder.getView<ImageView>(com.lxj.xpopup.R.id.iv_image).visibility =
                            View.GONE
                        holder.getView<TextView>(com.lxj.xpopup.R.id.tv_text).apply {
                            if (position == 3)
                                setTextColor(0xFEFF6969.toInt())
//                    else
//                        setTextColor(ResourcesCompat.getColor(resources, com.lxj.xpopup.R.color._xpopup_white_color, null))
                        }
                    }
                }
            adapter.apply {
                setOnItemClickListener(onItemClickListener = object : SimpleOnItemClickListener() {
                    override fun onItemClick(
                        view: View,
                        holder: RecyclerView.ViewHolder,
                        position: Int
                    ) {
                        selectListener?.onSelect(position, adapter.data[position])
                        if (popupInfo.autoDismiss) dismiss()
                    }

                })
            }
            recyclerView.adapter = adapter
            recyclerView.setBackgroundResource(R.drawable.radius_5)
        }
    }

}