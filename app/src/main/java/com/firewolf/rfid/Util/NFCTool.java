package com.firewolf.rfid.Util;

import android.annotation.SuppressLint;
import android.bluetooth.BluetoothDevice;
import android.content.ComponentName;
import android.content.ServiceConnection;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;

import com.dk.bleNfc.BleManager.BleManager;
import com.dk.bleNfc.BleManager.ScannerCallback;
import com.dk.bleNfc.BleNfcDeviceService;
import com.dk.bleNfc.DeviceManager.BleNfcDevice;
import com.dk.bleNfc.DeviceManager.ComByteManager;
import com.dk.bleNfc.DeviceManager.DeviceManager;
import com.dk.bleNfc.DeviceManager.DeviceManagerCallback;
import com.dk.bleNfc.Exception.DeviceNoResponseException;
import com.dk.bleNfc.Tool.StringTool;
import com.dk.bleNfc.card.Mifare;
import com.dk.bleNfc.BleManager.*;
import com.firewolf.rfid.Manager.QueueManager;

import org.jetbrains.annotations.NotNull;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class NFCTool {
    public static final int PERMISSION_REQUEST_COARSE_LOCATION = 1;
    private BleNfcDeviceService mBleNfcDeviceService;
    private BleNfcDevice bleNfcDevice;
    private Scanner mScanner;

    //private StringBuffer msgBuffer;
    private BluetoothDevice mNearestBle = null;
    private Lock mNearestBleLock = new ReentrantLock();// 锁对象
    private int lastRssi = -100;
    private String deviceName;
    private NFCDeviceStatusListener nfcStatus;
    private QueueManager qm;
    public interface NFCDeviceStatusListener {
        void onDeviceDisconnection();
        void onDeviceSearching();
        void onDeviceConnected();
        void onDeviceHousekeeping(double v);
    }

    public NFCTool(String deviceName, QueueManager qm){
        this.deviceName = deviceName;
        this.qm = qm;
        //msgBuffer = new StringBuffer();
    }

    public void setDeviceStatusListener(NFCDeviceStatusListener n){
        nfcStatus = n;
    }

/*    @Override
    protected void onResume() {
        super.onResume();
        if (mBleNfcDeviceService != null) {
            mBleNfcDeviceService.setScannerCallback(scannerCallback);
            mBleNfcDeviceService.setDeviceManagerCallback(deviceManagerCallback);
        }
    }
    @Override
    public void onDestroy() {
        super.onDestroy();

        unbindService(mServiceConnection);
    }*/
    public void resume(){
        if (mBleNfcDeviceService != null) {
            mBleNfcDeviceService.setScannerCallback(scannerCallback);
            mBleNfcDeviceService.setDeviceManagerCallback(deviceManagerCallback);
        }
    }

    // Code to manage Service lifecycle.
    public final ServiceConnection mServiceConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName componentName, IBinder service) {
            mBleNfcDeviceService = ((BleNfcDeviceService.LocalBinder) service).getService();
            bleNfcDevice = mBleNfcDeviceService.bleNfcDevice;
            mScanner = mBleNfcDeviceService.scanner;
            mBleNfcDeviceService.setDeviceManagerCallback(deviceManagerCallback);
            mBleNfcDeviceService.setScannerCallback(scannerCallback);

            //开始搜索设备
            System.out.println("開始搜索附近設備");
            //10s housekeeping
            Thread bleMonitor = new Thread(new Runnable() {
                @Override
                public void run() {
                    while (mBleNfcDeviceService != null) {
                        searchNearestBleDevice();

                        try {
                            Thread.sleep(1000);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                        nfcDeviceNoticeHousekeeping(); //10s housekeeping
                    }
                }
            });
            bleMonitor.start();
        }

        @Override
        public void onServiceDisconnected(ComponentName componentName) {
            mBleNfcDeviceService = null;
        }
    };

    //搜索到設備時進行的callback
    private ScannerCallback scannerCallback = new ScannerCallback() {
        @Override
        public void onReceiveScanDevice(BluetoothDevice device, int rssi, byte[] scanRecord) {
            super.onReceiveScanDevice(device, rssi, scanRecord);

            //抓到設備就停止
            //搜索蓝牙设备
            if ( mNearestBle == null &&
                    bleNfcDevice.isConnection() == BleManager.STATE_DISCONNECTED &&
                    (scanRecord != null) && //从广播数据中过滤掉其它蓝牙设备
                    (StringTool.byteHexToSting(scanRecord).contains("017f5450")) && //特定設備
                    deviceName.equals(device.getName())) {  //指定名稱

                System.out.println("停止搜索");
                mScanner.stopScan();
                System.out.println("Activity搜到设备：" + device.getName()
                        + " 信号强度：" + rssi
                        + " scanRecord：" + StringTool.byteHexToSting(scanRecord));

                mNearestBleLock.lock();
                try {
                    mNearestBle = device;
                }finally {
                    mNearestBleLock.unlock();
                }
                lastRssi = rssi;
            }
        }

        @Override
        public void onScanDeviceStopped() {
            super.onScanDeviceStopped();
        }

    };

    //设备操作类回调
    private DeviceManagerCallback deviceManagerCallback = new DeviceManagerCallback() {
        @Override
        public void onReceiveConnectBtDevice(boolean blnIsConnectSuc) {
            super.onReceiveConnectBtDevice(blnIsConnectSuc);
            if (blnIsConnectSuc) {
                System.out.println("Activity設備連接成功");
                System.out.println("設備\n");
                System.out.println("   名稱: "+ bleNfcDevice.getDeviceName() + "\n");
                System.out.println("SDK版本: " + BleNfcDevice.SDK_VERSIONS + "\n");

                //连接上后延时500ms后再开始发指令
                try {
                    Thread.sleep(500L);
                    handler.sendEmptyMessage(3);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }else{
                /*nfcStatus.onDeviceDisconnection();*/
            }
        }

        @Override
        public void onReceiveDisConnectDevice(boolean blnIsDisConnectDevice) {
            super.onReceiveDisConnectDevice(blnIsDisConnectDevice);
            System.out.println("Activity设备断开链接");
            nfcStatus.onDeviceDisconnection();
        }

        @Override
        public void onReceiveConnectionStatus(boolean blnIsConnection) {
            super.onReceiveConnectionStatus(blnIsConnection);
            System.out.println("Activity设备链接状态回调");
        }

        @Override
        public void onReceiveInitCiphy(boolean blnIsInitSuc) {
            super.onReceiveInitCiphy(blnIsInitSuc);
        }

        @Override
        public void onReceiveDeviceAuth(byte[] authData) {
            super.onReceiveDeviceAuth(authData);
        }

        @Override
        //寻到卡片回调
        public void onReceiveRfnSearchCard(boolean blnIsSus, int cardType, byte[] bytCardSn, byte[] bytCarATS) {
            super.onReceiveRfnSearchCard(blnIsSus, cardType, bytCardSn, bytCarATS);
            if (!blnIsSus || cardType == BleNfcDevice.CARD_TYPE_NO_DEFINE) {
                return;
            }

            final int cardTypeTemp = cardType;
            new Thread(new Runnable() {
                @Override
                public void run() {
                    boolean isReadWriteCardSuc;
                    try {
                        if (bleNfcDevice.isAutoSearchCard()) {
                            //如果是自动寻卡的，寻到卡后，先关闭自动寻卡
                            bleNfcDevice.stoptAutoSearchCard();
                            isReadWriteCardSuc = readWriteCardDemo(cardTypeTemp);
                            //读卡结束，重新打开自动寻卡
                            startAutoSearchCard();
                        }
                        else {
                            isReadWriteCardSuc = readWriteCardDemo(cardTypeTemp);
                            //如果不是自動尋卡，就開啟他
                            startAutoSearchCard();

                            //如果不是自动寻卡，读卡结束,关闭天线
                            //bleNfcDevice.closeRf();
                        }

                        //打开蜂鸣器提示读卡完成
                        bleNfcDevice.openBeep(1, 1, 2);  //读写卡成功快响3声

                    } catch (DeviceNoResponseException e) {
                        e.printStackTrace();
                    }
                }
            }).start();
        }

        @Override
        public void onReceiveRfmSentApduCmd(byte[] bytApduRtnData) {
            super.onReceiveRfmSentApduCmd(bytApduRtnData);
            System.out.println("Activity接收到APDU回调：" + StringTool.byteHexToSting(bytApduRtnData));
        }

        @Override
        public void onReceiveRfmClose(boolean blnIsCloseSuc) {
            super.onReceiveRfmClose(blnIsCloseSuc);
        }

        @Override
        //按键返回回调
        public void onReceiveButtonEnter(byte keyValue) {
            if (keyValue == DeviceManager.BUTTON_VALUE_SHORT_ENTER) { //按键短按
                System.out.println("Activity接收到按键短按回调");
            }
            else if (keyValue == DeviceManager.BUTTON_VALUE_LONG_ENTER) { //按键长按
                System.out.println("Activity接收到按键长按回调");
            }
        }
    };

    //开始自动寻卡
    private boolean startAutoSearchCard() throws DeviceNoResponseException {
        //打开自动寻卡，200ms间隔，寻M1/UL卡
        boolean isSuc = false;
        int falseCnt = 0;
        do {
            isSuc = bleNfcDevice.startAutoSearchCard((byte) 20, ComByteManager.ISO14443_P4);
        }while (!isSuc && (falseCnt++ < 10));
        if (!isSuc){
            //msgBuffer.delete(0, msgBuffer.length());
            //msgBuffer.append("不支持自动寻卡！\r\n");
        }
        return isSuc;
    }

    //读写卡Demo
    private boolean readWriteCardDemo(int cardType) {
        switch (cardType) {
            case DeviceManager.CARD_TYPE_MIFARE:   //寻到Mifare卡
                //System.out.println("拿到Mifare卡");
                //System.out.println(Thread.currentThread().getName());
                final Mifare mifare = (Mifare) bleNfcDevice.getCard();
                if (mifare != null) {
                    String uid = mifare.uidToString();
                    System.out.println("UID:"+uid);
                    qm.getBQueue().offer(uid);

                }
                break;
        }
        return true;
    }

    //搜索最近的设备并连接
    private void searchNearestBleDevice() {

        if (!mScanner.isScanning() && (bleNfcDevice.isConnection() == BleManager.STATE_DISCONNECTED)) {
            System.out.println("開始搜尋");
            mScanner.startScan(0);
            nfcStatus.onDeviceSearching();
            mNearestBleLock.lock();
            try {
                mNearestBle = null;
            }finally {
                mNearestBleLock.unlock();
            }
            lastRssi = -100;

            int searchCnt = 0;

            while ((mNearestBle == null)
                    && (searchCnt < 10000)
                    && (mScanner.isScanning())
                    && (bleNfcDevice.isConnection() == BleManager.STATE_DISCONNECTED)) {
                searchCnt++;
                try {
                    Thread.sleep(1);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }

            if(mScanner.isScanning()) {
                System.out.println("掃描結束，無找到設備");
                nfcStatus.onDeviceDisconnection();
                mScanner.stopScan();
            }
            try {
                //等待assign
                Thread.sleep(500);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            if (bleNfcDevice.isConnection() == BleManager.STATE_DISCONNECTED) {
                mNearestBleLock.lock();
                try {
                    if (mNearestBle != null) {//connecting
                        bleNfcDevice.requestConnectBleDevice(mNearestBle.getAddress());
                    } else {
                        //not find device
                    }
                }finally {
                    mNearestBleLock.unlock();
                }
            }
        }
    }

    private void nfcDeviceNoticeHousekeeping(){
        double v = 0;

        if (bleNfcDevice.isConnection() == BleManager.STATE_CONNECTED) {
            try {
                v = bleNfcDevice.getDeviceBatteryVoltage();
                //System.out.println("Housrkeeping "+ v);
            } catch (DeviceNoResponseException e) {
                e.printStackTrace();
            }
            nfcStatus.onDeviceHousekeeping(v);
        }
    }

    @SuppressLint("HandlerLeak")
    private Handler handler = new Handler() {
        @Override
        public void handleMessage(@NotNull Message msg) {

            switch (msg.what) {
                case 3:
                    new Thread(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                /*获取设备版本号*/
                                /*获取设备电池电压*/
                                byte versions = bleNfcDevice.getDeviceVersions();
                                double voltage = bleNfcDevice.getDeviceBatteryVoltage();
                                System.out.println("設備\n");
                                System.out.println("   電量: "+ bleNfcDevice.getDeviceBatteryVoltage() + "\n");
                                System.out.println("   版本: "+ versions + "\n");
                                /*开启自动寻卡*/
                                //msgBuffer.append("\n开启自动寻卡...\r\n");
                                //开始自动寻卡
                                startAutoSearchCard();
                                nfcStatus.onDeviceConnected();
                            } catch (DeviceNoResponseException e) {
                                e.printStackTrace();
                            }
                        }
                    }).start();
                    break;
            }
        }
    };
}
