package com.firewolf.rfid.Util;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.LocationManager;
import android.provider.Settings;

import androidx.appcompat.app.AppCompatActivity;

public class GPSUtil{
        public static void openGPS(final Context context){

            if ( !gpsIsOPen(context) ) {
                System.out.println("log:" + "GPS未打开！");

                final AlertDialog.Builder normalDialog =
                        new AlertDialog.Builder(context);
                normalDialog.setTitle("未打開位置功能");
                normalDialog.setMessage("連接NFC設備需要位置功能");
                normalDialog.setPositiveButton("前往開啟",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                //...To-do
                                // 转到手机设置界面，用户设置GPS
                                Intent intent = new Intent(
                                        Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                                ((AppCompatActivity)context).startActivityForResult(intent, 0); // 设置完成后返回到原来的界面
                            }
                        });
                normalDialog.setNegativeButton("不開",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                //...To-do
                            }
                        });
                // 显示
                normalDialog.show();
            }
        }
        /**
         * 判断GPS是否开启，GPS或者AGPS开启一个就认为是开启的
         * @param context
         * @return true 表示开启
         */
        public static boolean gpsIsOPen(final Context context) {
            LocationManager locationManager
                    = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
            // 通过GPS卫星定位，定位级别可以精确到街（通过24颗卫星定位，在室外和空旷的地方定位准确、速度快）
            boolean gps = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
            // 通过WLAN或移动网络(3G/2G)确定的位置（也称作AGPS，辅助GPS定位。主要用于在室内或遮盖物（建筑群或茂密的深林等）密集的地方定位）
            boolean network = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
            if (gps || network) {
                return true;
            }

            return false;
        }
}
