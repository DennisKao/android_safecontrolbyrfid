package com.firewolf.rfid.module

//修改此dataclass 需增加DAOTool版號，避免用戶端資料庫版本不同而閃退。
open class FiremenDBInfo(
    //RFID Card Id
    var id: String = "",
    //真名
    var name: String = "",
    //編號
    var number: String = "",
    //呼號
    var callSign: String = "",
    //單位
    var department: String = "",
    //氧瓶時間
    var O2TotalTime: Int = 1800,//秒
    //氧瓶第一階段警報時間
    var O2AlertTime: Int = 1200,//秒
    //氧瓶第二階段警報時間
    var O2CriticalTime: Int = 600,//秒
    //是否為安全官(0 or 1)(f/t)
    var security: Boolean = false,
    //Channgel
    var channel: Int = 0){

    fun toFiremenInfo(): FiremenInfo{
        val fmInfo = FiremenInfo()
        fmInfo.id = id
        fmInfo.name = name
        fmInfo.number = number
        fmInfo.callSign = callSign
        fmInfo.department = department
        fmInfo.O2TotalTime = O2TotalTime
        fmInfo.O2AlertTime = O2AlertTime
        fmInfo.O2CriticalTime = O2CriticalTime
        fmInfo.security = security
        fmInfo.channel = channel

        return fmInfo
    }
}
class FiremenInfo(

    //氧氣暫停時間 -1 無暫停 0 曾經暫停
    var O2PauseTimestamp: Long = -1,
    //氧氣耗盡時間
    var O2DepleteTimestamp: Long = 0

): FiremenDBInfo() {

    val ts: Long
        get() = System.currentTimeMillis()

    //Notice 氧氣啟動時間戳
    var O2startTimestamp: Long = 0
        set(startTs) {
            O2DepleteTimestamp = startTs + O2TotalTime * 1000
            field = startTs
        }

    //Notice 氧氣警告時間戳
    val O2AlertTimestamp: Long
        get() = O2DepleteTimestamp - O2AlertTime * 1000

    //Notice 氧氣臨界時間戳
    val O2CriticalTimestamp: Long
        get() = O2DepleteTimestamp - O2CriticalTime * 1000

    //Notice 氧氣剩餘時間
    var O2RemainingTime: Int = 0
        get() {
            return if(isPaused)
                //暫停時就會將剩下的時間存起來，所以取本身即可，恢復後就會取出來用
                field
            else
                 //如果是runtime 那只要抓取結束TS扣掉當前TS就能得到目前剩多久時間s
                (O2DepleteTimestamp - ts).toInt() /1000
        }

    val didPaused: Boolean
        get() = this.O2PauseTimestamp != -1L

    val isPaused: Boolean
        get() = this.O2PauseTimestamp > 0L

    fun pause(){
        O2PauseTimestamp = ts
        O2RemainingTime = (O2DepleteTimestamp - O2PauseTimestamp).toInt() / 1000
    }

    fun resume(){
        O2DepleteTimestamp = ts + O2RemainingTime * 1000
        O2PauseTimestamp = 0
    }

    fun editTime(remainingTimeSec: Int){
        if(isPaused){
            // 設定自訂剩餘時間
            O2RemainingTime = remainingTimeSec

        }else{
            // run time
            // 直接更改結束TS
            O2DepleteTimestamp = ts + remainingTimeSec * 1000
        }

    }

    fun init(_ts: Long) {
        O2startTimestamp = _ts
        O2PauseTimestamp = 0
    }
}


