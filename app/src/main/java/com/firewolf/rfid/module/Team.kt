package com.firewolf.rfid.module


import androidx.databinding.ObservableField
import com.firewolf.rfid.R

open class TeamObserver{
    val obName: ObservableField<String> = ObservableField()
    val obLeader: ObservableField<FiremenInfo> = ObservableField()
    val obStarImg: ObservableField<Int> = ObservableField()
    val obEnableMode: ObservableField<Boolean> = ObservableField()
}


interface Team{
    val memList: Map<String, FiremenInfo>
    val size: Int

    fun getInfo(): TeamInfo
}

data class TeamInfo(
    val id: String,
    var name: String,
    var leader: FiremenInfo?
)

class TeamEntity(
    private val teamInfo: TeamInfo): Team{ // memId, FiremenInfo
    private var _memList = linkedMapOf<String, FiremenInfo>()

    override val memList: Map<String, FiremenInfo>
        @Synchronized
        get() = _memList.toMap()
    override val size: Int
        @Synchronized
        get() = memList.size
    override fun getInfo(): TeamInfo = teamInfo
    
    fun addMember(fmInfo: FiremenInfo) = _memList.put(fmInfo.id, fmInfo)
    
    fun removeMember(memId: String) = _memList.remove(memId)
}