package com.firewolf.rfid

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.res.ColorStateList
import android.graphics.Color
import android.net.Uri
import android.os.Bundle
import android.text.SpannableStringBuilder
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.ImageView
import android.widget.PopupWindow
import android.widget.TextView
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.bigkoo.pickerview.builder.OptionsPickerBuilder
import com.bigkoo.pickerview.listener.OnOptionsSelectListener
import com.bumptech.glide.Glide
import com.contrarywind.view.WheelView
import com.firewolf.rfid.Manager.FiremenManager
import com.firewolf.rfid.Manager.InOutManager
import com.firewolf.rfid.Manager.QueueManager
import com.firewolf.rfid.module.FiremenDBInfo
import com.firewolf.rfid.module.FiremenInfo
import com.firewolf.rfid.Util.DAOTool
import com.firewolf.rfid.VM.RegVM
import com.theartofdev.edmodo.cropper.CropImage
import com.theartofdev.edmodo.cropper.CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE
import com.theartofdev.edmodo.cropper.CropImageView
import kotlinx.android.synthetic.main.activity_register.*
import kotlinx.coroutines.*
import java.io.File


class RegisterActivity : globalActivity() {

    private lateinit var vm: RegVM
    private val fm: FiremenManager by lazy {(applicationContext as mApp).fm}
    private val qm: QueueManager by lazy {(applicationContext as mApp).qm}
    private val iom: InOutManager by lazy {(applicationContext as mApp).iom}
    private val pop: PopupWindow by lazy {PopupWindow(this)}
    private var avatarUri: Uri? = null
    private var RQueueJob: Job? = null
    private var regState: STATE = STATE.UNSENSE
    private val defaultFMInfo = FiremenInfo()

    enum class STATE{
        UNSENSE,
        REGISTER,
        MODIFY
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register)

        vm = ViewModelProvider(this)[RegVM::class.java]
        vm = RegVM()
        vm.rfid.observe(this, Observer {
            regRfid.text = SpannableStringBuilder(it)
        })

        this.overridePendingTransition(R.anim.fade_out, R.anim.fade_in)

        regName.nextFocusDownId = regNum.id
        regDepartment.nextFocusDownId = regChannel.id

        setUIBaseInfo(View.INVISIBLE, false, defaultFMInfo, STATE.UNSENSE, false)

        regTotalTimeText.setOnClickListener(OnClickRegTimeText(0))
        regAlertTimeText.setOnClickListener(OnClickRegTimeText(1))
        regCriticalTimeText.setOnClickListener(OnClickRegTimeText(2))

        registerEvent()
    }

    private fun registerEvent(){
        back.setOnClickListener{ this.finish() }
        regChoiceAvatar.setOnClickListener {openImgSelector()}
        regConfirmBtn.setOnClickListener(OnConfirmBtnClick())
    }

    private fun setAvatar(id: String, view: ImageView){
        val dir = getAvatarAlbumPath()
        val file =
            if(id.isEmpty()) R.drawable.avatar_no_img
            else {
                // maybeTODO: 需要增加讀取外部空間權限判斷
                val _f = File(dir, "ava_$id.jpg")
                if (_f.exists()) {
                    _f
                } else {
                    R.drawable.avatar_no_img
                }
            }
        Glide.with(this).load(file).into(view)

    }

    private fun openImgSelector() {
        CropImage.activity()
            .setGuidelines(CropImageView.Guidelines.ON)
            .setCropShape(CropImageView.CropShape.OVAL)
            .setFixAspectRatio(true)
            .setCropMenuCropButtonTitle("儲存")
            .start(this);
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        // Notice: 將回調的頭像顯示到UI
        if (requestCode == CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            val result = CropImage.getActivityResult(data)
            if (resultCode == Activity.RESULT_OK) {
                val resultUri = result.uri
                avatarUri = resultUri
                Glide.with(this).load(resultUri).into(regAvatar)
                regAvatar.setImageURI(resultUri)
            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                //val error = result.error
                Toast.makeText(this, "拍攝頭像異常", Toast.LENGTH_SHORT).show()
            }
        }
    }

    override fun onResume() {
        super.onResume()
        //初始化

        RQueueJob?.cancel()
        RQueueJob = CoroutineScope(Dispatchers.IO).launch {
            while(true){
                val cardId = qm.RQueue.poll()
                delay(200)
                cardId?.let {_cardId ->
                    var visible: Int = View.INVISIBLE
                    var showAvatar = false

                    val fmInfo = fm.getFireMen(_cardId)?.apply {
                        // NOTICE: 修改模式
                        visible = View.VISIBLE
                        regState = STATE.MODIFY
                        showAvatar = true
                    } ?: (FiremenDBInfo(cardId, "大米", "134", "老鼠", "水管", 5*60, 3*60, 1*60, false, 0).toFiremenInfo()).also {
                        // notice 先行替使用者輸入資料
                        // notice 註冊模式
                        visible = View.INVISIBLE
                        regState = STATE.REGISTER
                        showAvatar = false
                    }

                    launch(Dispatchers.Main)  {
                        this@RegisterActivity.setUIBaseInfo(visible, true, fmInfo, regState, showAvatar)
                        val imm = (this@RegisterActivity.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager)

                        // Notice: 焦點設定以及彈出鍵盤
                        if(regState == STATE.REGISTER){
                            regName.requestFocus()
                            imm.showSoftInput(regName, InputMethodManager.SHOW_IMPLICIT)
                        }
                        else{
                            imm.hideSoftInputFromWindow(regRfid.windowToken, 0)
                        }
                    }
                }
            }
        }
        iom.switchMode(InOutManager.STATE.REGISTER)
    }

    override fun onPause() {
        this.overridePendingTransition(R.anim.fade_out, R.anim.fade_in)
        RQueueJob?.cancel()
        super.onPause()
    }

    //Notice 轉換 分:秒
    fun toMinSec(sec: Int): String{
        val tMin = sec/60
        val tSec = sec%60
        return "$tMin:$tSec"
    }

    //Notice 轉換 秒
    fun toSec(MinSec: String): Int{
        val list = MinSec.split(":")
        val min = list[0].toInt()
        val sec = list[1].toInt()
        return min*60 + sec
    }

    // Notice: 顯示UI基本資訊
    //  依順序輸入為
    //  是否顯示編輯狀態bar
    //  是否開放欄位可編輯
    //  要預先輸入欄位的fmInfo
    //  當前狀態(enum STATE)
    //  是否載入使用者頭像 false為使用默認頭像
    private fun setUIBaseInfo(regEditBarVisible: Int, allowEditable: Boolean, fmInfo: FiremenInfo, state: STATE, showAvatar: Boolean){
        regEditBar.visibility = regEditBarVisible
        allowTextEditable(allowEditable)
        setFmInfoToUI(fmInfo)
        regState = state
        switchBtnStyle(regState)

        setAvatar(if(showAvatar) fmInfo.id else "", regAvatar)
    }

    // notice 切換確認按鈕的樣式
    private fun switchBtnStyle(controlMode: STATE){
        when(controlMode){
            STATE.UNSENSE -> {
                regConfirmBtn.text = "請感應卡片"
                regConfirmBtn.setBackgroundColor(Color.argb(50, 255, 255, 255))
                regConfirmBtn.setTextColor(Color.argb(50, 255, 255, 255))
            }
            STATE.REGISTER -> {
                regConfirmBtn.text = "註冊"
                regConfirmBtn.setBackgroundColor(Color.rgb(254, 104, 48))
                regConfirmBtn.setTextColor(Color.WHITE)
            }
            STATE.MODIFY -> {
                regConfirmBtn.text = "確定修改"
                regConfirmBtn.setBackgroundColor(Color.rgb(71, 208, 208))
                regConfirmBtn.setTextColor(0xFE2c323e.toInt())
            }
            else ->{}
        }
    }

    // NOTICE: 將資料上載到螢幕中
    private fun setFmInfoToUI(fmInfo: FiremenInfo){
        regRfid.text = SpannableStringBuilder(fmInfo.id)
        regName.text = SpannableStringBuilder(fmInfo.name)
        regNum.text = SpannableStringBuilder(fmInfo.number)
        regCallSign.text = SpannableStringBuilder(fmInfo.callSign)
        regDepartment.text = SpannableStringBuilder(fmInfo.department)

        regTotalTimeText.text = toMinSec(fmInfo.O2TotalTime)
        regAlertTimeText.text = toMinSec(fmInfo.O2AlertTime)
        regCriticalTimeText.text = toMinSec(fmInfo.O2CriticalTime)
        regSecurity.isChecked = fmInfo.security
        regChannel.text = SpannableStringBuilder(fmInfo.channel.toString())
    }

    // NOTICE: 從UI拿取人員資料
    private fun getConfirmInfoFromUI(): FiremenInfo?{
        if(regName.text.isEmpty() ||
                regNum.text.isEmpty() ||
                regCallSign.text.isEmpty() ||
                regDepartment.text.isEmpty() ||
                regChannel.text.isEmpty()){
            return null
        }else{

            val fmDBInfo =  FiremenDBInfo(
                        id = regRfid.text.toString(),
                        name = regName.text.toString(),
                        number = regNum.text.toString(),
                        callSign = regCallSign.text.toString(),
                        department = regDepartment.text.toString(),
                        O2TotalTime = toSec(regTotalTimeText.text.toString()),
                        O2AlertTime = toSec(regAlertTimeText.text.toString()),
                        O2CriticalTime = toSec(regCriticalTimeText.text.toString()),
                        channel = regChannel.text.toString().toInt(),
                        security = regSecurity.isChecked
            )
            return fmDBInfo.toFiremenInfo()
        }
    }

    // Notice: 變更編輯狀態
    private fun allowTextEditable(enable: Boolean){
        regChoiceAvatar.isEnabled = enable
        regName.isEnabled = enable
        regNum.isEnabled = enable
        regCallSign.isEnabled = enable
        regDepartment.isEnabled = enable
        regTotalTimeText.isEnabled = enable
        regAlertTimeText.isEnabled = enable
        regCriticalTimeText.isEnabled = enable
        regSecurity.isEnabled = enable
        regChannel.isEnabled = enable
    }

    // Notice: 確認按鈕事件
    private inner class OnConfirmBtnClick(): View.OnClickListener{

        override fun onClick(v: View?) {
            try {
                when(regState){
                    //Notice 未感應票卡
                    STATE.UNSENSE -> {
                        Toast.makeText(this@RegisterActivity, "請感應卡片", Toast.LENGTH_SHORT).show()
                    }
                    //Notice Confirm
                    else -> {
                        getConfirmInfoFromUI()?.let{ fmInfo ->
                            // Notice 全都有填寫
                            DAOTool.MemberDatabase.getInstance(this@RegisterActivity)?.apply {
                                if(fm.contains(regRfid.text.toString())){
                                    // Notice 編輯
                                    CoroutineScope(Dispatchers.IO).launch { memberDao().updateMember(DAOTool.Firemen(fmInfo.id, fmInfo)) }
                                    Toast.makeText(this@RegisterActivity, "編輯完成", Toast.LENGTH_SHORT).show()
                                }else{
                                    // Notice 註冊
                                    CoroutineScope(Dispatchers.IO).launch { memberDao().insertNewMember(DAOTool.Firemen(fmInfo.id, fmInfo)) }
                                    Toast.makeText(this@RegisterActivity, "註冊完成", Toast.LENGTH_SHORT).show()
                                }

                                fm.regUpdFiremen(fmInfo)
                                avatarUri?.let { uri->
                                    CoroutineScope(Dispatchers.IO).launch { saveAvatar(fmInfo.id, uri) }
                                }

                                CoroutineScope(Dispatchers.Main).launch {
                                    setUIBaseInfo(View.INVISIBLE, false, defaultFMInfo, STATE.UNSENSE, false)
                                }
                            }
                        } ?: run{
                            //Notice ㄇㄉ 你沒填寫完啊
                            Toast.makeText(this@RegisterActivity, "請補全資訊", Toast.LENGTH_SHORT).show()
                        }
                    }
                }
            } catch (e: Exception){
                e.printStackTrace()
            }
        }
    }

    // Notice: 處理時間設定按鈕
    inner class OnClickRegTimeText(private val type: Int) : View.OnClickListener{

        var max = 5999
        var min = 0

        override fun onClick(v: View?) {
            val targetView = v as TextView
            var originColor: ColorStateList = targetView.textColors
            val imm = (this@RegisterActivity.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager)
            // 關閉鍵盤
            imm.hideSoftInputFromWindow(regRfid.windowToken, 0)

            targetView.setTextColor(0xff80cbc4.toInt())

            when(type){
                0 -> {
                    min = toSec(regAlertTimeText.text.toString())
                }
                1 -> {
                    max = toSec(regTotalTimeText.text.toString())
                    min = toSec(regCriticalTimeText.text.toString())
                }
                2 -> {
                    max = toSec(regAlertTimeText.text.toString())
                }
            }

            val pvOptions = OptionsPickerBuilder(this@RegisterActivity,
                OnOptionsSelectListener { options1, options2, _, v -> //返回的分别是三个级别的选中位置

                    val optionSec =
                        if(max <= 60)
                            options1
                        else
                            options1 * 60 + options2

                    when(type){
                        0 ->{
                            if(optionSec < min){
                                Toast.makeText(this@RegisterActivity, "氣瓶總時間低於警戒時間", Toast.LENGTH_LONG).show()
                            }
                        }
                        1 -> {
                            if(optionSec < min){
                                regCriticalTimeText.text = toMinSec(optionSec)
                            }
                        }
                    }

                    targetView.text = toMinSec(optionSec)

                })
                .setSubmitText("確定") //确定按钮文字
                .setCancelText("取消") //取消按钮文字
                .setTitleText("時間修改") //标题
                .setSubCalSize(20) //确定和取消文字大小
                .setTitleSize(24) //标题文字大小
                .setContentTextSize(22) //滚轮文字大小
                .setTitleColor(Color.BLACK) //标题文字颜色
                .setSubmitColor(Color.BLUE) //确定按钮文字颜色
                .setCancelColor(Color.BLUE) //取消按钮文字颜色
                .setLineSpacingMultiplier(2f)
                .setDividerType(WheelView.DividerType.WRAP)
                .setCyclic(false, false, false) //循环与否
                .setSelectOptions(0, 0) //设置默认选中项
                .isAlphaGradient(false)
                .isCenterLabel(false)
                .setOutSideColor(Color.TRANSPARENT)

            val pvView by lazy{pvOptions.build<Int>()}

            if(max <= 60){
                pvOptions.setLabels("秒", "", "")
                pvView.setNPicker((0..max).toList(), null, null) //添加数据源
            }else{
                val sec : MutableList<List<Int>> = ArrayList()
                repeat(max/60){i ->
                    sec.add((0..60).toList())
                }
                sec.add((0..max%60).toList())

                pvOptions.setLabels("分", "秒", "")
                pvView.setPicker((0..max/60).toList(), sec, null) //添加数据源
            }

            pvView.setOnDismissListener {
                targetView.setTextColor(originColor)
            }

            pvView.show()
        }
    }
}
