package com.firewolf.rfid.ViewControl.MainActivity

import android.app.Activity
import android.content.Context
import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import com.bigkoo.pickerview.builder.OptionsPickerBuilder
import com.bigkoo.pickerview.listener.OnOptionsSelectListener
import com.contrarywind.view.WheelView
import com.firewolf.rfid.Manager.InOutManager
import com.firewolf.rfid.Manager.QueueManager
import com.firewolf.rfid.R
import com.firewolf.rfid.Util.XPopup.MemOptionPopupView
import com.firewolf.rfid.module.FiremenInfo
import com.firewolf.rfid.module.Team
import com.lxj.xpopup.XPopup
import com.lxj.xpopup.interfaces.OnSelectListener
import com.lxj.xpopup.interfaces.XPopupCallback
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.mem_avatar.view.*
import kotlinx.coroutines.*
import java.text.SimpleDateFormat
import java.util.*


class MemControlAdapter(
    val MAX_MEMBER_COLUMN: Int,
    val iom: InOutManager,
    val qm: QueueManager,
    val adapterTeam: Team
) : RecyclerView.Adapter<MemControlAdapter.Holder>(){

    enum class NotifyType {
        STAR,

    }

    enum class AvatarStyle{
        EMPTY,
        PAUSE,
        NORMAL,
        ALERT,
        CRITICAL,
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): Holder {
        //(parent.parent as View).teamLeader.text = "123123123"
        val view = LayoutInflater.from(parent.context).inflate(R.layout.mem_avatar, parent, false)
        val holder =  Holder(view)
        return holder
    }

    override fun getItemCount(): Int {
        val total = adapterTeam.size

        val row =
            when(total % MAX_MEMBER_COLUMN){
                0 -> {
                    if(total == 0)
                           1
                    else
                        total/MAX_MEMBER_COLUMN
                }
                else -> total/MAX_MEMBER_COLUMN + 1
            } * MAX_MEMBER_COLUMN
        return row
    }

    override fun onViewDetachedFromWindow(holder: Holder) {
        super.onViewDetachedFromWindow(holder)
        holder.stopHousekeeping()
    }


    override fun onBindViewHolder(holder: Holder, position: Int, payloads: MutableList<Any>) {
        if(payloads.isEmpty()){
            super.onBindViewHolder(holder, position, payloads)
        }else{
            when(payloads[0]){
                NotifyType.STAR -> {
                    holder.refreshStar()
                }
            }
        }
    }

    override fun onBindViewHolder(holder: Holder, position: Int) {
        holder.upd(adapterTeam)
    }

    fun removeMember(memIndex: Int) {
        val count = adapterTeam.size
        if(count % MAX_MEMBER_COLUMN == 0 && count >= MAX_MEMBER_COLUMN){
            notifyItemRangeRemoved(itemCount, MAX_MEMBER_COLUMN)
            if(memIndex < count) {//非末排首位
                notifyItemRemoved(memIndex)
                notifyItemChanged(itemCount - 1)
            }else{}
        }else{
            notifyItemRemoved(memIndex)
            notifyItemInserted(itemCount -1 )
        }
    }

    inner class Holder(private val view: View) : RecyclerView.ViewHolder(view) {
        private val avaImg: ImageView = view.avaImg
        private val inTimeText: TextView = view.avaInTime
        private val memStarImg = view.avaStar
        private val nameText: TextView = view.avaName
        private val text = view.avaText
        private val context: Context = view.context

        private var team: Team = this@MemControlAdapter.adapterTeam
        private val fmInfo: FiremenInfo?
            get() = team.memList.values.toList().getOrNull(adapterPosition)
        private var timerHousekeeper: Job? = null

        val parent by lazy { view.parent.parent as View }

        init {
            view.setOnClickListener {
                println("Click")
            }
            view.setOnLongClickListener {
                fmInfo?.let {fmInfo ->
                    XPopup
                        .Builder(context)
                        .hasShadowBg(true)
                        .isCenterHorizontal(true)
                        .setPopupCallback(MemOptionPopUpViewCallBack())
                        .atView(avaImg)
                        .asCustom(object: MemOptionPopupView(context){
                            init {
                                data = if(fmInfo.isPaused)
                                    listOf("恢復", "修改氣瓶", "指定為組長", "刪除隊員")
                                else
                                    listOf("暫停", "修改氣瓶", "指定為組長", "刪除隊員")
                                selectListener = MemOptionPopUpViewSelectListener(fmInfo)
                            }
                        }).show()
                }
                false
            }
        }

        fun upd(team: Team){
            this.team = team
            fmInfo?.let { fmInfo ->
                val timeStr = SimpleDateFormat("HH:mm.ss").format(Date(fmInfo.O2startTimestamp))

                nameText.text = fmInfo.name
                inTimeText.text = timeStr
                inTimeText.visibility = View.VISIBLE

                refreshStar()

                avaImg.setImageResource(R.drawable.avatar_no_img)

                startHousekeeping()

            } ?: run{
                // maybeTODO: 之後可能需要改背景就好 然後增加padding
                switchAvatarStyle(AvatarStyle.EMPTY)

                nameText.text = ""
                inTimeText.visibility = View.INVISIBLE
                memStarImg.visibility = View.INVISIBLE
                avaImg.setImageDrawable(null)
            }
        }

        fun stopHousekeeping(){
            timerHousekeeper?.cancel()
        }

        // Notice: 處理氧氣剩餘時間顏色顯示
        fun startHousekeeping(){
            timerHousekeeper?.let{th ->
                if(th.isActive)
                    th.cancel()
            }
            timerHousekeeper = CoroutineScope(Dispatchers.IO).launch{
                while(true){
                    fmInfo?.let {fmInfo ->
                        refreshLayoutColor(fmInfo) }
                    delay(1000)
                }
            }
        }

        private fun refreshLayoutColor(_fmInfo: FiremenInfo){
            if(_fmInfo.isPaused){
                _fmInfo.apply {
                    switchAvatarStyle(AvatarStyle.PAUSE)
                }

            }else {
                val nowTime = System.currentTimeMillis()

                when {
                    nowTime > _fmInfo.O2CriticalTimestamp -> {
                        switchAvatarStyle(AvatarStyle.CRITICAL)
                        //stopHousekeeping() //Notice 時間到期後關閉HouseKeeping 此功能目前不開啟
                    }
                    nowTime > _fmInfo.O2AlertTimestamp -> {
                        switchAvatarStyle(AvatarStyle.ALERT)
                    }
                    else -> {
                        switchAvatarStyle(AvatarStyle.NORMAL)
                    }
                }
            }
        }

        fun refreshStar(){
            fmInfo?.let{
                memStarImg.visibility = if(team.getInfo().leader == it)
                    View.VISIBLE
                else
                    View.INVISIBLE
            }
        }

        private fun switchAvatarStyle(style: AvatarStyle){
            when(style){
                AvatarStyle.EMPTY -> {
                    setAvatarUIColor(R.drawable.avatar_color_w, Color.WHITE)
                }
                AvatarStyle.PAUSE -> {
                    setAvatarUIColor(R.drawable.avatar_color_pause, Color.WHITE)
                }
                AvatarStyle.NORMAL -> {
                    setAvatarUIColor(R.drawable.avatar_color_b, Color.BLACK)
                }
                AvatarStyle.ALERT -> {
                    setAvatarUIColor(R.drawable.avatar_color_y, Color.BLACK)
                }
                AvatarStyle.CRITICAL -> {
                    setAvatarUIColor(R.drawable.avatar_color_r, Color.WHITE)
                }
            }
        }

        private fun setAvatarUIColor(layoutColor: Int, fontColor: Int){
            view.setBackgroundResource(layoutColor)
            text.setTextColor(fontColor)
            inTimeText.setTextColor(fontColor)
            nameText.setTextColor(fontColor)
        }

        private fun setLeader(){
            if(fmInfo == null){
                Toast.makeText(context, "隊員已出火場，無法設定為隊長", Toast.LENGTH_SHORT).show()
                return
            }
            val recycler = (view.context as Activity).recycle
            val tmHolder = recycler.getChildViewHolder(parent) as TeamControlAdapter.Holder

            team.getInfo().leader = fmInfo
            tmHolder.refreshLeader()
        }

        inner class MemOptionPopUpViewCallBack: XPopupCallback {
            override fun onDismiss() {
                (context as Activity).recycle.setOnScrollChangeListener { v, scrollX, scrollY, oldScrollX, oldScrollY ->  }
                iom.switchMode(InOutManager.STATE.INOUT)}
            override fun onCreated() {
                (context as Activity).recycle.setOnScrollChangeListener { v, scrollX, scrollY, oldScrollX, oldScrollY ->
                    println("$scrollY")
                }
                iom.switchMode(InOutManager.STATE.STOP)
            }
            override fun onBackPressed(): Boolean = false
            override fun beforeShow() {}
            override fun onShow() {
            }
        }

        inner class MemOptionPopUpViewSelectListener(private val fmInfo: FiremenInfo): OnSelectListener{

            override fun onSelect(position: Int, text: String?) {
                when (position) {
                    // Notice: Pause|Resume
                    0 -> {
                        stopHousekeeping()
                        if(fmInfo.isPaused){
                            Toast.makeText(context, "恢復", Toast.LENGTH_SHORT).show()
                            //resume
                            fmInfo.resume()

                        }else{
                            Toast.makeText(context, "暫停", Toast.LENGTH_SHORT).show()
                            //pause
                            fmInfo.pause()
                        }

                        startHousekeeping()
                    }
                    // Notice: 修改氣瓶
                    1 -> {
                        val min = fmInfo.O2TotalTime/60
                        val sec = fmInfo.O2TotalTime%60
                        val pvOptions = OptionsPickerBuilder(context,
                            OnOptionsSelectListener { options1, options2, _, v -> //返回的分别是三个级别的选中位置
                                if(min == 0)
                                    fmInfo.editTime(options1)
                                else
                                    fmInfo.editTime(options1*60+options2)
                            })
                            .setSubmitText("確定") //确定按钮文字
                            .setCancelText("取消") //取消按钮文字
                            .setTitleText("${fmInfo.name} 氣瓶時間修改") //标题
                            .setSubCalSize(20) //确定和取消文字大小
                            .setTitleSize(24) //标题文字大小
                            .setContentTextSize(22) //滚轮文字大小
                            .setTitleColor(Color.BLACK) //标题文字颜色
                            .setSubmitColor(Color.BLUE) //确定按钮文字颜色
                            .setCancelColor(Color.BLUE) //取消按钮文字颜色
                            .setLineSpacingMultiplier(2f)
                            .setDividerType(WheelView.DividerType.WRAP)
                            .setCyclic(false, false, false) //循环与否
                            .setSelectOptions(0, 0) //设置默认选中项
                            .isAlphaGradient(true)
                            .isCenterLabel(false)

                        val pvView by lazy{pvOptions.build<Int>()}

                        if(min == 0){
                            pvOptions.setLabels("秒", "", "")
                            pvView.setNPicker((0..sec).toList(), null, null) //添加数据源
                        }
                        else{
                            pvOptions.setLabels("分", "秒", "")
                            pvView.setNPicker((0..min).toList(), (0..sec).toList(), null) //添加数据源
                        }
                        pvView.show()
                    }
                    // Notice: 指定為組長
                    2 -> {
                        setLeader()
                    }
                    // Notice: 刪除人員
                    3 -> {
                        iom.switchMode(InOutManager.STATE.INOUT)
                        qm.BQueue.offer(fmInfo.id)
                    }
                }
            }
        }
    }
}