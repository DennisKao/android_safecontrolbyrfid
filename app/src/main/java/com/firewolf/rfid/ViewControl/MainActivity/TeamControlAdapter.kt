package com.firewolf.rfid.ViewControl.MainActivity

import android.content.DialogInterface
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.LinearLayout.LayoutParams
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.RecycledViewPool
import androidx.recyclerview.widget.SimpleItemAnimator
import com.firewolf.rfid.MainActivity
import com.firewolf.rfid.Manager.InOutManager
import com.firewolf.rfid.Manager.QueueManager
import com.firewolf.rfid.Manager.TeamManager
import com.firewolf.rfid.Manager.TeamManager.TeamManagerListener
import com.firewolf.rfid.R
import com.firewolf.rfid.ViewControl.MainActivity.TeamControlAdapter.PayLoadEvent.*
import com.firewolf.rfid.databinding.MemMaintainLayoutBinding
import com.firewolf.rfid.module.Team
import com.firewolf.rfid.module.TeamEntity
import com.firewolf.rfid.module.TeamObserver
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.mem_maintain_layout.view.*
import kotlinx.android.synthetic.main.mem_maintain_layout.view.rfidSelected
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch


/*人員管制系統-各小組內容控制，RFID Cursor Display、隊長*/
class TeamControlAdapter(
    private val tm: TeamManager,
    private val iom: InOutManager,
    private val qm: QueueManager
) : RecyclerView.Adapter<TeamControlAdapter.Holder>() {

    private val MAX_MEMBER_COLUMN = 6
    private val viewPool = RecycledViewPool()

    sealed class PayLoadEvent{
        data class MEMBER_ADDED(val memIndex: Int): PayLoadEvent()
        data class MEMBER_REMOVED(val memIndex: Int): PayLoadEvent()
        object REFRESH_RFID_FLAG : PayLoadEvent()
    }

    init {
        viewPool.setMaxRecycledViews(0, 48)//設置此參數解決卡頓問題，造成卡頓的原因在於cell最大數過低，導致cell被回收

        repeat(10){it ->
            tm.createTeam("第{$it}面")
        }
        tm.selectedTeam = tm.teamList[0]

        tm._listener = object: TeamManagerListener{
            override fun onTeamDeleted(index: Int) {
                if(index in 0 until itemCount){
                    CoroutineScope(Dispatchers.Main).launch {
                        delay(100)
                        notifyItemRemoved(index)
                    }
                }
            }

            override fun onTeamCreated(team: Team, index: Int) {
                CoroutineScope(Dispatchers.Main).launch {
                    delay(100)
                    notifyItemInserted(index)
                }
            }

            override fun onMemberAddedToTeam(memIndex: Int, teamIndex: Int) {
                CoroutineScope(Dispatchers.Main).launch {
                    delay(100)
                    notifyItemChanged(teamIndex, MEMBER_ADDED(memIndex))
                }
            }

            override fun onMemberRemovedFromTeam(memIndex: Int, teamIndex: Int) {
                CoroutineScope(Dispatchers.Main).launch {
                    delay(100)
                    notifyItemChanged(teamIndex, MEMBER_REMOVED(memIndex))
                }
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): Holder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.mem_maintain_layout, parent, false)
        return Holder(view)
    }

    override fun getItemCount(): Int = tm.teamList.size

    override fun onBindViewHolder(holder: Holder, position: Int, payloads: MutableList<Any>) {
        if(payloads.isEmpty())
            // Notice: Normal refresh
            super.onBindViewHolder(holder, position, payloads)
        else{
            // Notice: Solved Payload Event
            payloads.getOrNull(0)?.let { e ->
                when(e){
                    is MEMBER_ADDED -> {
                        val memIndex = e.memIndex
                        val adapter = holder.recyclerView.adapter as MemControlAdapter
                        adapter.notifyItemChanged(memIndex)
                    }

                    is MEMBER_REMOVED -> {
                        val memIndex = e.memIndex
                        val adapter = holder.recyclerView.adapter as MemControlAdapter

                        adapter.removeMember(memIndex)
                        holder.refreshLeader()
                    }

                    is REFRESH_RFID_FLAG -> {
                        holder.refreshRFIDFlag()
                    }
                    else -> {}
                }
            }
        }
    }

    override fun onBindViewHolder(holder: Holder, position: Int) {//螢幕刷洞時會call

        tm.teamList.getOrNull(position)?.let { team ->

            holder.recyclerView.apply {
                setRecycledViewPool(viewPool)
                layoutManager = GridLayoutManager(context, MAX_MEMBER_COLUMN)
                adapter = MemControlAdapter(MAX_MEMBER_COLUMN, iom, qm, team)
                val sa = (this.itemAnimator) as SimpleItemAnimator
                sa.supportsChangeAnimations = false
            }

            holder.upd(team)
        }
    }

    inner class Holder(private val view: View) : RecyclerView.ViewHolder(view) {
        val recyclerView : RecyclerView = view.memRecycler
        val editBtn = view.editTeamBtn
        val delBtn = view.delTeamBtn
        val teamName = view.teamName
        val teamStar = view.teamStar
        val teamLeader = view.teamLeader

        val context = (view.context as MainActivity)

        lateinit var team: Team
        private val rfidSelectTeam: TextView = context.rfidSelectTeam


        init{//預期能做到 如果重複點擊不會做一樣的操作（省電？
            // Notice: Holder上的按鈕事件註冊
            view.setOnClickListener {
                // RFID Select
                selectPos(adapterPosition)
            }
            editBtn.setOnClickListener {
                EditDialog().show()
            }
            delBtn.setOnClickListener {
                if(team.size > 0){
                    memberNotLogoutAlertDialog.show()
                }else
                    DelConfirmDialog().show()
            }
        }

        private fun selectPos(pos: Int){
            if(pos in 0 until itemCount){
                println("selectPos: pos : $pos")
                tm.selectedTeam = team
                notifyItemRangeChanged(0, itemCount, REFRESH_RFID_FLAG)
            }else{
                println("selectPos: invalid pos : $pos")
            }
        }

        private fun disableRFIDFlag(){
            view.rfidSelected.visibility = View.INVISIBLE
        }

        private fun enableRFIDFlag(){
            rfidSelectTeam.text = view.teamName.text
            view.rfidSelected.visibility = View.VISIBLE
        }

        fun refreshRFIDFlag(){
            when (team) {
                tm.selectedTeam -> enableRFIDFlag()
                else            -> disableRFIDFlag()
            }
        }

        fun refreshLeader() {
            recyclerView.adapter?.let { adapter ->
                adapter.notifyItemRangeChanged(0, adapter.itemCount, MemControlAdapter.NotifyType.STAR)
            }
            team.getInfo().leader?.let {fmInfo ->
                teamStar.setImageResource(R.drawable.star_full)
                teamLeader.text = fmInfo.name
            } ?: run {
                teamStar.setImageResource(R.drawable.star_empty)
                teamLeader.text = ""
            }
        }

        fun removeTeam(){
            if(tm.removeTeam(teamId = team.getInfo().id)){
                // Notice: 如果刪除的是RFID選擇的本身
                if(team == tm.selectedTeam) {
                    tm.selectedTeam = null
                    context.rfidSelectTeam.text = "None"
                }
            }
        }

        fun rename(name: String){
            team.getInfo().name = name
            teamName.text = name
            if(team == tm.selectedTeam)
                context.rfidSelectTeam.text = name
        }

        fun upd(team: Team) {
            this.team = team
            teamName.text = this.team.getInfo().name

            refreshLeader()
            refreshRFIDFlag()
        }

        inner class DelConfirmDialog: AlertDialog.Builder(context){
            init {
                setTitle("刪除隊伍")
                setMessage("確認要刪除${teamName.text}？")
                val confirm = DialogInterface.OnClickListener { _, _ ->
                    if(team.size > 0)
                        memberNotLogoutAlertDialog.show()
                    else
                        removeTeam()
                }
                setPositiveButton("確認", confirm)
                setNeutralButton("取消", DialogInterface.OnClickListener{ _, _ -> })
            }
        }

        inner class EditDialog: AlertDialog.Builder(context){
            init{
                val editText = EditText(context)
                val lp = LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT)
                editText.layoutParams = lp
                setView(editText)
                setTitle("更改隊伍名稱")
                setMessage("輸入新的隊伍名稱")
                val confirm = DialogInterface.OnClickListener { d, _ ->
                    if(editText.text.isEmpty())
                        Toast.makeText(context, "隊伍名稱不能為空", Toast.LENGTH_SHORT).show()
                    else{
                        rename(editText.text.toString())
                    }
                }
                setPositiveButton("確認", confirm)
                setNeutralButton("取消", DialogInterface.OnClickListener{ _, _ -> })
            }
        }

        val memberNotLogoutAlertDialog = object: AlertDialog.Builder(context){
            init {
                setTitle("刪除小隊失敗")
                setMessage("尚有人員在小隊中，請先撤出所有隊員")
            }
        }
    }
}